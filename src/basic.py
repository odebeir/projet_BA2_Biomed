# coding=utf-8

import cv2 as cv
import matplotlib.pylab as mpl

# ouvre la caméra par défaut
cam = cv.VideoCapture(0)

# vérifie que la caméra est correctement ouverte et disponible pour des acquisitions
assert cam.isOpened()

# demande une image
ok, img = cam.read()

# vérifie le statut
assert ok

# affiche l'image
cv.imshow('color', img)

# affiche le type et la taille de l'image
print img.dtype, img.shape

# convertit l'image en niveaux de gris et l'affiche
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
cv.imshow('gray', gray)

# affiche l'histogramme des niveaux de gris
mpl.hist(gray.ravel(), bins=256, normed=True)
mpl.show()

# attend que l'utilisateur appuye sur un touche quelconque
cv.waitKey(0)
